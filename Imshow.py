#!/usr/bin/env python
#
# Copyright (C) 2019 Jacopo Nespolo <j.nespolo@gmail.com>
#
# This file is part of mplhacks.
#
# mplhacks is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# mplhacks is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You may find the complete text of the GNU General Public license, version
# 3 at <https://www.gnu.org/licenses/gpl.txt>

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider

closest = lambda v, _x: np.argmin(np.abs(_x - v))
getdiff = lambda _x: _x[1] - _x[0]

class Imshow(object):
    def __init__(self, x, y, data, *args, **kwargs):
        self.x = x
        self.y = y
        self.data = data

        self.fig = plt.figure()

        axes_heights = [3, 1, 0.2]
        axes_widths = [1]
        gs = self.fig.add_gridspec(nrows=3, ncols=1,
                width_ratios=axes_widths,
                height_ratios=axes_heights)

        self.ax_xy = self.fig.add_subplot(gs[0, 0])
        self.ax_xx = self.fig.add_subplot(gs[1, 0])
        self.ax_slider_x = self.fig.add_subplot(gs[2, 0])

        self.ax_xy.imshow(self.data, aspect='auto',
                extent=(np.min(self.x), np.max(self.x),
                        np.min(self.y), np.max(self.y)),
                origin='lower'
                )


        initial_index = closest(0, self.x)
        self.xplot, = self.ax_xx.plot(self.y, self.data[:, initial_index])
        self.ax_xx.set_ylim(np.min(self.data), np.max(self.data))

        self.slider_x = Slider(self.ax_slider_x, 'x slice',
                valmin=np.min(x), valmax=np.max(x), valinit=0,
                valstep=getdiff(self.x))
        self.slider_x.on_changed(self.update)
        plt.tight_layout()

    def update(self, val):
        idx = closest(val, self.x)
        newy = self.data[:, idx]
        self.xplot.set_ydata(newy)
        self.fig.canvas.draw_idle()



if __name__ == "__main__":

    x = np.linspace(-np.pi, np.pi, 50)
    y = np.linspace(-4, 4, 100)

    X, Y = np.meshgrid(x, y)
    data = np.cos(X) * np.exp(-(Y**2)) + np.random.normal(0, 0.1, X.shape)


#    data = np.random.uniform(0, 1, (x.size, y.size))
    Imshow(x, y, data)
    plt.show()

    exit(0)
